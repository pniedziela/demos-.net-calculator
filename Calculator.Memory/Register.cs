﻿using System;

using Calculator.Contract;

namespace Calculator.Memory
{
    class Register : IRegister
    {
        double value = 0;

        public double Retrive()
        {
            return value;
        }

        public void Store(double value)
        {
            this.value = value;
        }

        public void Clear()
        {
            value = 0;
        }
    }
}
