﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Contract
{
    public interface IMemoryOperations
	{
		void MemoryClear();
		void MemoryRetrive();
		void MemoryStore();
      
		void Reset();
	}
}
