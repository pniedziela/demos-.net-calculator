﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Contract
{
    public interface IMemory
    {
        IRegister CreateRegister(string name);

        IRegister GetRegister(string name);

        IRegister GetOrCreateRegister(string name);
    }
}
