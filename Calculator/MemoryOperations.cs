﻿using System;

using Calculator.Contract;

namespace Calculator.Device
{
    class MemoryOperations : IMemoryOperations
    {
        internal IRegister accumulator { private get; set; }
        internal IRegister memory { private get; set; }
        internal IDisplay display { private get; set; }

        public void MemoryRetrive() 
        { 
            double result = memory.Retrive();
            accumulator.Store(result);
            display.Value = result.ToString();
        }

        public void MemoryStore() 
        { 
            memory.Store(accumulator.Retrive()); 
        }

        public void MemoryClear() 
        { 
            memory.Clear(); 
        }

        public void Reset()
        {
            accumulator.Clear();
            memory.Clear();
            display.Value = 0.ToString();
        }
    }
}
