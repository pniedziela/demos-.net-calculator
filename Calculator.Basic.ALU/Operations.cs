﻿using Calculator.Contract;

namespace Calculator.ALU.Basic
{
    class Add : IOperation
    {
        public string Name
        {
            get { return "Add"; }
        }

        public double Calculate(double number1, double number2)
        {
            return number1 + number2;
        }
    }

    class Sub : IOperation
    {
        public string Name
        {
            get { return "Sub"; }
        }

        public double Calculate(double number1, double number2)
        {
            return number1 - number2;
        }
    }

    class Mul : IOperation
    {
        public string Name
        {
            get { return "Mul"; }
        }

        public double Calculate(double number1, double number2)
        {
            return number1 * number2;
        }
    }

    class Div : IOperation
    {
        public string Name
        {
            get { return "Div"; }
        }

        public double Calculate(double number1, double number2)
        {
            return number1 / number2;
        }
    }
}
